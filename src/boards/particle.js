import Particle from "particle-io";
import Five from "johnny-five";

export const board = new Five.Board({
  io: new Particle({
    token: process.env.PARTICLE_TOKEN,
    deviceId: process.env.PHOTON_1,
  })
});
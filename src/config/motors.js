import {
  MOTOR_A_PWR,
  MOTOR_A_DIR,
  MOTOR_B_PWR,
  MOTOR_B_DIR
} from '../constants';

export const INITIAL_SPEED = 40;

export const MOTOR_A = {
  pins: {
    pwm: MOTOR_A_PWR,
    dir: MOTOR_A_DIR,
  },
  invertPWM: true
}

export const MOTOR_B = {
  pins: {
    pwm: MOTOR_B_PWR,
    dir: MOTOR_B_DIR,
  },
  invertPWM: true
}
import {
  ProximityUFrontPin,
  ProximityUBackLPin,
  ProximityUBackRPin
} from '../constants';

export const proximityUFrontConf = {
  controller: "HCSR04",
  pin: ProximityUFrontPin,
  freq: 500,
}

export const proximityUBackLConf = {
  controller: "HCSR04",
  pin: ProximityUBackLPin,
  freq: 500,
}

export const proximityUBackRConf = {
  controller: "HCSR04",
  pin: ProximityUBackRPin,
  freq: 500,
}
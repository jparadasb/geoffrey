export const MOTOR_A_PWR = "D2";
export const MOTOR_A_DIR = "D3";
export const MOTOR_B_PWR = "D0";
export const MOTOR_B_DIR = "D1";

export const FORWARD = 'motor:forward';
export const REVERSE = 'motor:reverse';
export const LEFT = 'motor:left';
export const RIGHT = 'motor:right';
export const STOP = 'motor:stop';
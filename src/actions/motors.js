
import { INITIAL_SPEED } from '../config';

const DIR = {
  TOLEFT: 'left',
  TORIGHT: 'right',
  FORWARD: 'forward',
  REVERSE: 'reverse'
} 
let direction = '';
let lastDirection = '';

export function forward(motors) {
  direction = DIR.FORWARD
  motors.forward(INITIAL_SPEED);
}

export function reverse(motors) {
  direction = DIR.REVERSE
  motors.reverse(INITIAL_SPEED);
}

export function stop(motors) {
  if (direction === DIR.TOLEFT || direction === DIR.TORIGHT) {
    if (lastDirection) {
      return lastDirection === DIR.FORWARD ? forward(motors) : reverse(motors);
    }
  };
  motors.stop()
  direction = '';
}

export function right(motors) {
  lastDirection = direction;
  direction = DIR.TORIGHT
  motors[0].forward(INITIAL_SPEED);
  motors[1].reverse(INITIAL_SPEED);
}

export function left(motors) {
  lastDirection = direction;
  direction = DIR.TOLEFT;
  motors[1].forward(INITIAL_SPEED);
  motors[0].reverse(INITIAL_SPEED);
}
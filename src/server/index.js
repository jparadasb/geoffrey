import { Server } from 'http';
import express from 'express';
import socketIO from 'socket.io';
import path from 'path';
import five from 'johnny-five';

import routes from './routes';
import events from './events';
import { MOTOR_A, MOTOR_B } from '../config';
import { board } from '../boards';

const app = express();
const server = Server(app);
const io = socketIO.listen(server);

routes.forEach((route) => {
  app[route.method](route.path, route.action);
})

server.listen(9000);

board.on("ready", () => {
  const motors = new five.Motors([
    MOTOR_A, MOTOR_B
  ]);

  motors.stop();
  io.sockets.on('connection', (socket) => {
    events.forEach((event) => {
      socket.on(event.name, (data) => { event.action({...data, motors}) });
    })
  });
})

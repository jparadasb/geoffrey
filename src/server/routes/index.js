import babelify from 'express-babelify-middleware';
import path from 'path';
import fs from 'fs';

const dirBase = path.resolve();

const routes = [
  {
    method: 'get',
    path: '/',
    action: (req, res) => {
      fs.readFile(`${dirBase}/src/client/index.html`, (err, data) => {
        if (err) {
          res.writeHead(500);
          return res.end('Error loading index.html');
        }

        res.writeHead(200);
        res.end(data);
      });
    }
  },
  {
    method: 'use',
    path: '/js/bundle.js',
    action: babelify(`${dirBase}/src/client/js/app.js`)
  }
];

export default routes;
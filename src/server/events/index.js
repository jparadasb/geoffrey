import { FORWARD, REVERSE, LEFT, RIGHT, STOP } from '../../constants';
import { stop, forward, reverse, left, right } from '../../actions';

const events = [
  {
    name: STOP,
    action: ({motors}) => stop(motors)
  },
  {
    name: FORWARD,
    action: ({motors}) => forward(motors)
  },
  {
    name: REVERSE,
    action: ({motors}) => reverse(motors)
  },
  {
    name: LEFT,
    action: ({motors}) => left(motors)
  },
  {
    name: RIGHT,
    action: ({motors}) => right(motors)
  }
];

export default events;
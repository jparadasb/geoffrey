import jQuery from 'jquery';
import io from 'socket.io-client';

import { FORWARD, REVERSE, LEFT, RIGHT, STOP } from '../../constants';

const socket = io.connect();
let current = '';

jQuery(document).keydown((event) => {
  if (event.key === current) return;
  current = event.key;
  switch (event.key) {
    case 'ArrowUp':
      socket.emit(FORWARD);
      break;
    case 'ArrowDown':
      socket.emit(REVERSE);
      break;
    case 'ArrowLeft':
      socket.emit(LEFT);
      break;
    case 'ArrowRight':
      socket.emit(RIGHT);
      break;
    default:
      break;
  }
})

jQuery(document).keyup(() => {
  current = '';
  socket.emit(STOP)
})
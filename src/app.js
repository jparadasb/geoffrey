
var ProximityUFrontPin = 'D6';
var ProximityUBackRPin = 'D5';
var ProximityUBackLPin = 'D7';

var speed = 80;
var interval = 50;
var maxSpeed = 200;
var currentDir = 'stop'
var distance = 100;

var increaseSpeed = function() {
  if ((speed + interval) < maxSpeed) {
    speed = (maxSpeed - interval);
  } else {
    speed = maxSpeed;
  }
}

var decreaseSpeed = function() {
  if ((speed - interval) > 0) {
    speed = speed - interval
  } else {
    speed = 0;
  }
}

var getProximityData = function (data) {
  distance = this.cm;
}

board.on("ready", function () {
  var proximityUFront = new five.Proximity({
    controller: "HCSR04",
    pin: ProximityUFrontPin,
    freq: 500,
  });
  var proximityUBackL = new five.Proximity({
    controller: "HCSR04",
    pin: ProximityUBackLPin,
    freq: 500,
  });
  var proximityUBackR = new five.Proximity({
    controller: "HCSR04",
    pin: ProximityUBackRPin,
    freq: 500,
  });

  var proximityIR = new five.Sensor({
    pin: "A1",
    type: 'analog'
  });

  proximityIR.on("change", function () {
    console.log(this.scaleTo(0, 100));
  });

  proximityUFront.on("data", getProximityData);
  //proximityUBackL.on("data", getProximityData);
  //proximityUBackR.on("data", getProximityData);

  var motor = new five.Motors([{
    pins: {
      pwm: "D2",
      dir: "D3",
    },
    invertPWM: true
  },
  {
    pins: {
      pwm: "D0",
      dir: "D1",
    },
    invertPWM: true
  }]);

  board.repl.inject({
    motor: motor,
  });


  motor.stop();

  // set the motor going forward full speed (nothing happen)keypress(process.stdin);
  // listen for the "keypress" event
  process.stdin.on('keypress', function (ch, key) {
    switch (key.name || key.sequence) {
      case 'up':
        console.log(distance)
        motor.stop();
        motor.forward(speed);
        currentDir = 'forward';
        break;
      case 'down':
        motor.stop();
        motor.reverse(speed);
        currentDir = 'reverse';
        break;
      case 'right':
        motor.stop();
        motor[0].forward(speed);
        motor[1].reverse(speed);
        setTimeout(() => {
          motor[currentDir](speed);
        }, 500);
        break;
      case 'left':
        motor.stop();
        motor[1].forward(speed);
        motor[0].reverse(speed);
        setTimeout(() => {
          motor[currentDir](speed);
        }, 500);
        break;
      case '+':
        console.log(speed);
        increaseSpeed()
        console.log(speed);
        break;
      case '-':
        decreaseSpeed()
        break;
      case 'space':
        motor.stop();
        currentDir = 'stop'
        break;
      default:
        motor.stop();
        currentDir = 'stop'
    }
  });


  process.stdin.setRawMode(true);
  process.stdin.resume();
});


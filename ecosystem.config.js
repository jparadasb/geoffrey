module.exports = {
  apps: [
/*     {
      name: 'geoffrey',
      script: 'src/app.js',
      env: {
        COMMON_VARIABLE: 'true'
      },
      interpreter: "babel-node",
    }, */
    {
      name: 'geoffrey-server',
      script: 'src/server/index.js',
      watch: true,
      env: {
        COMMON_VARIABLE: 'true'
      },
      interpreter: "babel-node",
    }
  ],
};
